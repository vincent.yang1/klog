const RNFS = require('react-native-fs');
// require the module
// var RNFS = require('react-native-fs');

// create a path you want to write to
// :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
// but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable

module.exports = {
  uploadPath: [],
  uploadName: [],
  uploadTimeInterval: 30,
  baseUrl: '',
  log: function (type: string, fileId: string, data: string) {
    this.saveDataToLocalFile(type, fileId, data)
  },
  saveDataToLocalFile: function (type: string, fileId: string, data: string, changeType: string) {
    const filename = type + '_' + fileId + '_'  + (new Date().toISOString().split('T')[0].replace(/-/g,'_')) + '.json';
    const path = RNFS.DocumentDirectoryPath + '/' + filename
    RNFS.exists(path)
      .then((success) => {
        console.log(success)
        if (success && changeType === 'add') {
        // write the file
          RNFS.appendFile(path, ',' + data, 'utf8')
            .then((success) => {
              console.log('FILE WRITTEN!');
              if(this.uploadName.indexOf(filename) === -1){
                this.uploadName.push(filename)
                this.uploadPath.push({
                  uri: 'file://' + path,
                  name: filename,
                  mime: 'application/json',
                  type: 'multipart/form-data'
                })
              }

            })
            .catch((err) => {
              console.log(err.message);
            });
        } else {
        // write the file
          RNFS.writeFile(path, data, 'utf8')
            .then((success) => {
              console.log('FILE WRITTEN!');
              if(this.uploadName.indexOf(filename) === -1){
                this.uploadName.push(filename)
                this.uploadPath.push({
                  uri: 'file://' + path,
                  name: filename,
                  mime: 'application/json',
                  type: 'multipart/form-data'
                })
              }
            })
            .catch((err) => {
              console.log(err.message);
            });
        }
      })
  },
  startLogService: function (options) {
    this.uploadTimeInterval = options ? options.timeInterval : 8
    this.baseUrl = options ? options.baseUrl : ''
    if(!this.baseUrl) {
      throw new Error('baseUrl can not empty!')
    }
    setInterval(() => {
      this.uploadFileToServer()
    }, 1000 * this.uploadTimeInterval)
  },
  uploadFileToServer: function () {
    if (this.uploadPath.length > 0) {
      this.uploadPath.forEach((item) => {
        const file = item

        const body = new FormData()
        body.append('file', file)
        const starttime = Date.now()
        fetch(this.baseUrl + '/uploadFile', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data'
          },
          body
        })
        .then((success) => {

          console.log('uploaded file: ' + item.name)
          console.log("upload time: " + (Date.now() - starttime))
        })
        .catch((error) => {
          console.log(error.message);
        })

      })
    }
  }
}
